import { Given, Then, Before } from "cypress-cucumber-preprocessor/steps";

const url = "http://millidays.com/worldclock.html";

let myBeforeCount = 0;

// This verifies that the hooks work with bundling feature
// https://github.com/TheBrainFamily/cypress-cucumber-preprocessor/pull/234
/*Before(() => {
  expect(myBeforeCount).to.be.lessThan(2);
  myBeforeCount += 1;
});*/

Given(`i open the Millidays World Clock`, () => {
  cy.visit(url);
});

// This is the same step that we have in socialNetworks/Facebook/different.js, but you don't have to worry about collisions!
Then(`i see UTC Standard Time`, () => {
  cy.title().should("include", "The World Clock");
  cy.contains("h3", "UTC Standard Time");
  cy.get('body').find('h3')
  cy.get('body').contains("Timezones");
  cy.get('body > h3').contains("UTC Standard Time");
  cy.get('body > h3').contains("Millidays Earth Time");

  //cy.get('body.h3').should("include", "UTC Standard Time");
});