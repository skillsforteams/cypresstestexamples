import { Given, When, Then, Before } from "cypress-cucumber-preprocessor/steps";

const url = "http://ng.cyberrave.eu/partypics";

const apiUrl = "http://api.cyberrave.eu/api/partypics/gallery/list";




Given(`i use CyberRave live data`, () => {

});


// Cypress Intercepting Orginal Data URL 
Given(`i use CyberRave mock data`, () => {
  let mockData = [
    { "id": "580", "name": "CyperCore", "event_id": "911", "folder": "20191026-thunderdome", "date": "2019-10-26", "location_id": "73", "location_name": "Jaarbeus", "country": "nl", "city": "Utrecht" }, 
    { "id": "579",   "name": "Ground Zero Festival 2019", "event_id": "910", "folder": "20190831-ground-zero", "date": "2019-08-31", "location_id": "191",  "location_name": "Bussloo", "country": "nl", "city": "Wilp" },
    { "id": "577", "name": "Dr Peacock on Tour", "event_id": "908", "folder": "20190609-peacock-tour", "date": "2019-06-09", "location_id": "225", "location_name": "Atrium-Kiel", "country": "de", "city": "Kiel" }, 
    { "id": "576", "name": "Hardshock Festival 2019", "event_id": "907", "folder": "20190601-hardshock", "date": "2019-06-01", "location_id": "298", "location_name": "Hellendoorn Woods ", "country": "nl", "city": "Haarle" }, 
    { "id": "575", "name": "Pandemonium 2018", "event_id": "906", "folder": "20181201-pandemonium", "date": "2018-12-01", "location_id": "144", "location_name": "Sporthallen Zuid", "country": "nl", "city": "Amsterdam" }
  ];
  cy.intercept(apiUrl, mockData);
});

//Step using data from a Table
Given(`i use CyberRave table data`, (dataTable) => {
  let dataObj = [];

  dataTable.hashes().forEach((party) => {
    let partyArr =  {
    "id": party.id,
    "name" : party.name,
    "event": party.event_id,
    "folder": party.folder,
    "date": party.date,
    "location_id": party.location_id,
    "location_name": party.location_name,
    "country": party.country,
    "city": party.city
    }  
    dataObj.push(partyArr)
  });
  let jsonReturnString =  JSON.stringify(dataObj);
  cy.intercept(apiUrl, jsonReturnString);
});
When(`i visit CyberRave Partypics`, () => {
  cy.visit(url);
});

// This is the same step that we have in socialNetworks/Facebook/different.js, but you don't have to worry about collisions!
Then(`i see a Party named Thunderdome 2019`, () => {
  //selector all links with specific url  
  cy.get('a[href="/partypics/gallery/20191026-thunderdome"]').contains("Thunderdome 2019");
  //all links containing
  cy.get('a[href*="thunder"]').contains("Thunderdome 2019");
  //all links ending with
  cy.get('a[href$="thunderdome"]').contains("Thunderdome 2019");
  //all links beginning with
  cy.get('a[href^="/partypics/gallery/"]').contains("Thunderdome 2019");
});

// This is the same step that we have in socialNetworks/Facebook/different.js, but you don't have to worry about collisions!
Then(`i see a Party named {word}`, ( partyName ) => {
  //all links beginning with
  cy.get('a[href^="/partypics/gallery/"]').contains(partyName);
});

Then('I see Partys from the table', (partyTable) => {
  partyTable.hashes().forEach((party) => { 
    cy.get('a[href^="/partypics/gallery/"]').contains(party.name);
  });
});

