Feature: CyberRave Partypictures

    Scenario: Partylist from Original Site
        Given i use CyberRave live data
        When i visit CyberRave Partypics
        Then i see a Party named Thunderdome 2019


    Scenario: Partylist from Mock
        #This is the version for the mocked answer
        Given i use CyberRave mock data
        When i visit CyberRave Partypics
        Then i see a Party named CyperCore


    Scenario: Partylist from Table
        #This is the version for the table defined answer
        Given i use CyberRave table data
            | id | name        | event_id | folder             | date       | location_id | location_name   | country | city     |
            | 1  | tableCore   | 1        | 22220222-tablecore | 2222-02-22 | 3           | The holy Tunnel | se      | Valhalla |
            | 2  | tableTrance | 2        | 20330303-tablecore | 2033-03-03 | 3           | The holy Tunnel | se      | Valhalla |
        When i visit CyberRave Partypics
        Then i see a Party named tableCore
        Then i see a Party named tableTrance


    Scenario Outline: Partylist Outline Table as Table
        
        Given i use CyberRave table data
            | id | name        | event_id | folder               | date       | location_id | location_name   | country | city     |
            | 1  | OuterCore   | 1        | 22220222-outercore   | 2222-02-22 | 3           | The holy Tunnel | se      | Valhalla |
            | 2  | OuterTrance | 2        | 20330303-outertrance | 2033-03-03 | 3           | The holy Tunnel | se      | Valhalla |
        When i visit CyberRave Partypics
        Then I see Partys from the table
            | name   | folder   |
            | <name> | <folder> |
        Examples:
            | name        | folder               |
            | OuterCore   | 22220222-outercore   |
            | OuterTrance | 20330303-outertrance |

    Scenario Outline: Partylist Outline Table Named
        # Reusing the named step
        Given i use CyberRave table data
            | id | name        | event_id | folder               | date       | location_id | location_name   | country | city     |
            | 1  | OuterCore   | 1        | 22220222-outercore   | 2222-02-22 | 3           | The holy Tunnel | se      | Valhalla |
            | 2  | OuterTrance | 2        | 20330303-outertrance | 2033-03-03 | 3           | The holy Tunnel | se      | Valhalla |
        When i visit CyberRave Partypics
        Then i see a Party named <name>
            | <name> | <folder> |
        Examples:
            | name        | folder               |
            | OuterCore   | 22220222-outercore   |
            | OuterTrance | 20330303-outertrance |

